# Version

Base : tag 2.0.9.2

## Compilation

Build:
```
rm -f firmware*.bin; pio run && cp $(ls -rt1 .pio/build/STM32F103RET6_creality/firmware*.bin | tail -1) .
```

Build + Copy to SD card:
```
rm -f firmware*.bin; rm -f /Volumes/ENDER3/firmware*.bin  ; pio run && cp $(ls -rt1 .pio/build/STM32F103RET6_creality/firmware*.bin | tail -1) . && cp $(ls -rt1 .pio/build/STM32F103RET6_creality/firmware*.bin | tail -1) /Volumes/Ender3 && diskutil unmount /Volumes/Ender3
```

## Flashing Firmware

The bootloader which handles flashing new firmware on this board remembers the last filename you used.

Therefore, to flash the compiled firmware binary onto the board you must give the "`firmware.bin`" file on the SD card a unique name, different from the name of the previous firmware file, or you will be greeted with a blank screen on the next boot.
